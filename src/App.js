import NavigationBar from "./components/NavigationBar";
import PokimonCard from "./components/PokimonCard";
import HomePage from "./pages/HomePage";
import PokimonPage from "./pages/PokimonPage";
import ProductPage from "./pages/ProductPage";
import StateCounter from "./pages/StateCounter";
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter as BigRouter, Routes, Route } from "react-router-dom";
import UserProfilePage from "./pages/userpage/UserProfilePage";
import NotFoundPage from "./pages/NotFoundPage";
function App() {
  // get data froom api

  let products = [
    {
      productName: "Cocacola",
      imageUrl:
        "https://i.pinimg.com/736x/a6/0a/1c/a60a1c3a70bffd7d0ffdbff8469e65a8.jpg",
      price: 10,
      description:
        " shrub, Erythroxylon coca, native to the Andes, having simple, alternate leaves and small yellowish flowers. the dried leaves of this shrub, which are chewed for their stimulant properties and which yield cocaine and other alkaloids.",
    },
    {
      productName: "Samurai",
      imageUrl:
        "https://i.pinimg.com/736x/89/85/ec/8985ec3e27aa121afcd577fa94472653.jpg",
      price: 10,
      description:
        " shrub, Erythroxylon coca, native to the Andes, having simple, alternate leaves and small yellowish flowers. the dried leaves of this shrub, which are chewed for their stimulant properties and which yield cocaine and other alkaloids.",
    },
    {
      productName: "Fanta",
      imageUrl:
        "https://i.pinimg.com/originals/e8/65/09/e86509537771eeb3dc7673bbff3945ca.jpg",
      price: 10,
      description:
        " shrub, Erythroxylon coca, native to the Andes, having simple, alternate leaves and small yellowish flowers. the dried leaves of this shrub, which are chewed for their stimulant properties and which yield cocaine and other alkaloids.",
    },
    {
      productName: "Sting",
      imageUrl:
        "https://i.pinimg.com/736x/f0/90/a2/f090a2e55fe52a2ede990c7375bb9a32.jpg",
      price: 10,
      description:
        " shrub, Erythroxylon coca, native to the Andes, having simple, alternate leaves and small yellowish flowers. the dried leaves of this shrub, which are chewed for their stimulant properties and which yield cocaine and other alkaloids.",
    },
  ];

  return (
    <BigRouter>
      <Routes>
        <Route index element={<HomePage />} />
        <Route path="user" element={<UserProfilePage />} />
        <Route path="*" element={<NotFoundPage />} />
      </Routes>
    </BigRouter>
  );
}

export default App;

import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import { useState } from "react";

function PokimonCard() {
  let pokimonData = [
    {
      name: "PiChu",
      description:
        "Pichu, the Tiny Mouse Pokémon. A pre-evolved form of Pikachu. Despite their size, Pichu release bursts of electricity that can shock even humans. However, Pichu are unskilled at controlling their electricity, and sometimes release it by accident when",
      image:
        "https://i.pinimg.com/originals/14/74/b5/1474b5741a7ff5b0ce3c63945b00e466.jpg",
    },

    {
      name: "PikaChuu",
      description:
        "Pikachu was designed around the concept of electricity. They are creatures that have short, yellow fur with brown markings covering their backs and parts of their lightning bolt-shaped tails. They have black-tipped, pointed ears and red circular pouches on their cheeks, which can spark with electricity.",
      image:
        "https://i.pinimg.com/originals/dc/ab/b7/dcabb7fbb2f763d680d20a3d228cc6f9.jpg",
    },
    {
      name: "RaiChuu",
      description:
        "Raichu, a Mouse Pokémon of the Electric element. Raichu is the evolved form of Pikachu. It can shock with more than 100,000 volts, enough to render a Dragonite unconscious. Raichu, the evolved form of Pikachu.",
      image:
        "https://i.pinimg.com/originals/f0/e2/62/f0e2621176e9b6457a8bada09e04c8fd.jpg",
    },
  ];
  const [data, setData] = useState(pokimonData[0]);
  const [counter, setCounter] = useState(1);
  const handleTransformation = () => {
    setCounter(counter >= pokimonData.length - 1 ? 0 : counter + 1);
    console.log("Counter is : ", counter);
    setData(pokimonData[counter]);
  };
  return (
    <Card style={{ width: "18rem" }}>
      <Card.Img variant="top" src={data.image} />
      <Card.Body>
        <Card.Title> {data.name} </Card.Title>
        <Card.Text>{data.description}</Card.Text>
        <Button variant="warning" onClick={() => handleTransformation()}>
          Transform
        </Button>
      </Card.Body>
    </Card>
  );
}
export default PokimonCard;

import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";

function ProductCard({ productDetail }) {
  return (
    <Card style={{ width: "18rem" }}>
      <Card.Img variant="top" src={productDetail.imageUrl} />
      <Card.Body>
        <div className="d-flex justify-content-between">
          <Card.Title>{productDetail.productName}</Card.Title>
          <Card.Title className="text-warning">{productDetail.price}$</Card.Title>
        </div>

        <Card.Text>
          Some quick example text to build on the card title and make up the
          bulk of the card's content.
        </Card.Text>
        <Button variant="primary">View Details</Button>
      </Card.Body>
    </Card>
  );
}

export default ProductCard;

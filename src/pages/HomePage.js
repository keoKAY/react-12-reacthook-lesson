import React from 'react'

function HomePage() {
  return (
    <div className='container bg-warning '>

      <h1> HomePage</h1>
      <p> This is the description in the homepage </p>
      <button className="btn btn-primary">Click Me </button>
    </div>
  )
}

export default HomePage
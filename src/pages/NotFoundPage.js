import React from 'react'

function NotFoundPage() {
  return (
    <div>

        <h1 className='display-1 text-center text-danger'>
            404 Not Found !
        </h1>

    </div>
  )
}

export default NotFoundPage
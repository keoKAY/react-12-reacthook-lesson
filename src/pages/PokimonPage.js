import React, { useState } from "react";
import PokimonCard from "../components/PokimonCard";

const PokimonPage = () => {

  return (
    <div className="d-flex justify-content-center">
      <PokimonCard />
    </div>
  );
};

export default PokimonPage;

import React, { useState } from "react";
import ProductCard from "../components/ProductCard";

const ProductPage = ({ productData, message }) => {
  const [product, setProduct] = useState(productData);

  return (
    <div className="container">
      <div className="row ">
        {productData.map((product,index) => (
            <div key={index+product.productName} className="col-4 d-flex justify-content-center">
            <ProductCard productDetail = {product} />
          </div>
        ))}
      </div>
    </div>
  );
};

export default ProductPage;
